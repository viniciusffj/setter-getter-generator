module.exports = function(config) {
  config.set({
    basePath: '../',
    frameworks: ['jasmine'],

    files: [
      'js/test/*.js'
    ],
    browsers: ['Chrome'],

    // you can define custom flags
    customLaunchers: {
      Chrome_without_security: {
        base: 'Chrome'
      }
    }
  });
};
